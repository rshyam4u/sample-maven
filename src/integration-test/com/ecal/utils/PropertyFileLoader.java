package com.ecal.utils;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;

public class PropertyFileLoader {
	static{
        loadPropertiesFile();
    }
 
    public static PropertiesConfiguration config;
 
    /**
     * Loads Properties File.
     */
    private static void loadPropertiesFile() {
        try {
			config = new PropertiesConfiguration("application.properties");
		} 
        catch (ConfigurationException ex) {
			ex.printStackTrace();
		}
        config.setReloadingStrategy(new FileChangedReloadingStrategy());
    }
}
